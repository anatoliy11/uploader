(function() {
    let dropArea = document.getElementById('drop-area');
    let form = document.getElementById('form');
    let files;

    dropArea.addEventListener('dragenter', e => {
        e.preventDefault();
        dropArea.classList.add('dragged');
    });

    dropArea.addEventListener('dragover', e => e.preventDefault());

    dropArea.addEventListener('dragleave', e => {
        e.preventDefault();
        dropArea.classList.remove('dragged');
    });

    dropArea.addEventListener('dragend', e => {
        e.preventDefault();
        dropArea.classList.remove('dragged');
    });

    dropArea.addEventListener('drop', e => {
        e.preventDefault();
        dropArea.classList.remove('dragged');
        files = e.dataTransfer.files;
        if (files.length > 0) {
            dropArea.innerHTML = '<span class="file-name">' + files[0].name + '</span>';
        }
        
    });

    form.addEventListener('change', e => {
        files = e.target.files;
        if (files.length > 0) {
            dropArea.innerHTML = '<span class="file-name">' + files[0].name + '</span>';
        }
    })

    form.addEventListener('submit', e => {
        e.preventDefault();
        if (!files || !files.length) {
            return;
        }
        let formData = new FormData();
        formData.append('file', files[0]);
        fetch('upload', {
            method: 'POST',
            body: formData,
        })
            .then(res => res.json())
            .then(data => alert(JSON.stringify(data)))
            .catch(e => console.log(e));

    })
})()