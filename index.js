const express = require('express');
const app = express();
const upload = require('multer')({dest: 'public/upload/'});
const port = process.env.PORT || 3000;
const host = process.env.HOST || 'http://localhost:3000'

app.use(express.static('public'));

app.post('/upload', upload.single('file'), function(req, res) {
    res.send({
        path: require('path').join(__dirname, req.file.path),
        url: host + '/upload/' + req.file.filename,
    });
});

app.listen(port, () => console.log(`server is started on port ${port}`))